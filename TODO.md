# To Do

- xfwm4 theme: 
  - finish all the buttons
  - darken the color a bit


- infobars look all the same. What happens with buttons?

# GTK4

Window control buttons: separate them more
Calendar: style days and headers

fix undershoots so they look fine in gtk4





# references

This is because this theme is not generated with a css preprocessor, therefore some things need to be repeated (or I don't know how to DRY in the case of shadows or other things different than colors)

raised (normal buttons, sliders):

  box-shadow:
	inset 0px 0px 0px 1px alpha(@highlight_color, 0.5),
	0 1px 2px 0px alpha(@shadow_color, 0.5),
	0 0px 1px 1px alpha(@shadow_color, 0.3);


sunken (pressed buttons, entries):
  box-shadow:
	0px 1px 0px 0px alpha(@highlight_color, 0.5),
	inset 0 2px 2px 0px alpha(@shadow_color, 0.7),
	inset 0 0px 1px 1px alpha(@shadow_color, 0.3);

button:
	background-image:
    linear-gradient(to bottom, alpha(@highlight_color, 0.1), alpha(@shadow_color, 0.2));


# Oxygen adaptive

![small preview](previews/logo.png)

This is a skeuomorphic GTK3 theme that follows your KDE Plasma color scheme, the same way that Breeze GTK theme adapts to it. So if you want to change its colors, you only need to change KDE Plasma's color scheme, nothing else.

Do you want to keep using your venerable Oxygen or QtCurve widgets with Plasma, with any color scheme but can't find an appropriate GTK3 theme that matches it? Seek no more!

This theme is a modification of my [Pho-Earth-by-night](https://www.pling.com/p/1459383/), changing color names to match breeze's and some other improvements. At the same time, it is based on the wonderful pho series by GUILMOUR, such as Pho-Myrtus [here](https://www.pling.com/s/Gnome/p/1240355/). It has currently departed very far away from that theme.

This Oxygen Adaptive includes some predefined color schemes (that will work for GTK3 and 4, and are independent from Plasma themes). They're in the color_themes directory. By default, bright Pho Earth theme is enabled. To enable another theme, go to the directory where the theme is installed (typically: `~/.themes` or `~/.local/share/themes`) and execute the script change_theme.sh. This script will tell you which themes are available and let you chose one. This means that you can change this theme's colors regardless of current Plasma scheme (if there is one).

**By default, this theme will not try to follow current Plasma color scheme**. I had to do this so I could make Plasma theme work both for GTK3 and GTK4. To force this theme to honor current Plasma color scheme, you have to execute the script `toggle_plasma.sh`, following previous instructions too. You can also create new themes, it's quite easy: just copy one theme's file to another name and edit it: you just need to know how to define colors in RGB hexadecimal values, but any online color picker will help you with that.

Sadly, GTK2 can't follow plasma KDE color scheme because the trick used by Plasma KDE for GTK3 is with the file ~/.config/colors.css, but this only affects GTK3 theme, not GTK2 or GTK4. Currently GTK2 uses Pho-Earth-by-night's GTK2 fixed colors.

GTK4 version is still under construction. It will probably look good enough most times, but there are things to be fixed (still a lot of warnings and errors, for example). Please comment if there's anything to be fixed other than said errors and warnings. I'm still learning about it by myself, by trial and error (is there any documentation that explains most if not all the changes from GTK3? Please tell me in the comments, too).

Unfortunately, it seems the trick to make GTK3 use KDE Plasma's colors does not work for GTK4. The only thing I've done so far is the following: I've addedd a theme\_colors.css link at the root of the theme and both GTK3 and GTK4 themes read this file to use its colors. This link points to a CSS with a color theme in the color\_themes folder. It's a simple file, very easy and clear to edit (you just need how to define colors in RGB hexadecimal values, but any online color picker will help you with that). GTK3 will still adapt to KDE Plasma color scheme. Currently there are two themes, and you can chose which theme to you want to be active by running the script change\_theme.sh. You can add more themes, and the script will help you with chosing (or you can manually remove old link and create the new one, too)

The first 3 previews show current style. Other previews show an older style, but I've kept them so you can see how this adaptation to Plasma's color schemes works.

There is a matching xfwm4 theme too, which will adapt to current colors.

Enjoy!

(some keywords to help finding this theme: non-flat, 3D, 3-D, adaptive, adapts, multiple colors, multicolor, shadows, highlights)

## Big previews:

Current style:

![big preview with current style](previews/GTK3WF-1-default.png)
